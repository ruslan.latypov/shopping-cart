import axios from 'axios'

const instance = axios.create()

const getProducts = () => {
  return instance.get('data.json')
}

export default {
  getProducts
}
