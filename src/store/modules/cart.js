const state = {
  items: []
}

const getters = {
  cartProducts: (state, getters, rootState) => {
    return state.items.map(({ id, quantity }) => {
      const product = rootState.products.all.find(product => product.id === id)
      return {
        id: product.id,
        title: product.title,
        price: product.price,
        inventory: product.inventory,
        quantity
      }
    })
  },
  cartTotalPrice: (state, getters, rootState, rootGetters) => {
    return getters.cartProducts.reduce((accumulator, product) => {
      const price = rootGetters['products/price'](product)
      const total = accumulator + price * product.quantity
      return Math.round(total * 100) / 100
    }, 0)
  }
}

const actions = {
  addProduct: ({ state, commit }, product) => {
    if (product.inventory > 0) {
      const cartItem = state.items.find(item => item.id === product.id)
      if (!cartItem) {
        commit('pushProductToCart', { id: product.id })
      } else {
        commit('incrementItemQuantity', cartItem)
      }
      commit('products/decrementProductInventory', { id: product.id }, { root: true })
    }
  },
  removeProduct: ({ state, commit }, product) => {
    commit('removeProductFromCart', { id: product.id })
    commit('products/pushProductInventory', { id: product.id, quantity: product.quantity }, { root: true })
  }
}

const mutations = {
  pushProductToCart (state, { id }) {
    state.items.push({ id, quantity: 1 })
  },
  removeProductFromCart (state, { id }) {
    const cartItem = state.items.findIndex(item => item.id === id)
    state.items.splice(cartItem, 1)
  },
  incrementItemQuantity (state, { id }) {
    const cartItem = state.items.find(item => item.id === id)
    cartItem.quantity++
  },
  decrementItemQuantity (state, { id }) {
    const cartItem = state.items.find(item => item.id === id)
    cartItem.quantity--
  },
  updateItemQuantity (state, { id, quantity }) {
    const cartItem = state.items.find(item => item.id === id)
    cartItem.quantity = quantity
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
