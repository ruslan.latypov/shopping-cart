import api from '../../api'
import names from '@/names'

const state = {
  all: null,
  rate: 64.1213
}

const getters = {
  products: (state) => {
    return state.all.reduce((r, a) => {
      r[a.category] = [...r[a.category] || [], a]
      return r
    }, {})
  },
  price: (state) => ({ id }) => {
    const price = state.all.find(product => product.id === id).price * state.rate
    return Math.round(price * 100) / 100
  }
}

const actions = {
  getProducts: ({ commit, state, rootState }) => {
    api.getProducts().then(res => {
      const data = res.data.Value.Goods.map(item => {
        const product = {
          id: item.T,
          title: names[item.G].B[item.T].N,
          category: names[item.G].G,
          price: item.C,
          stock: item.P,
          inventory: item.P
        }

        const cartItem = rootState.cart.items.find(item => item.id === product.id)
        if (cartItem) {
          if (product.stock > cartItem.quantity) {
            product.inventory = product.stock - cartItem.quantity
          }
          if (product.stock <= cartItem.quantity) {
            product.inventory = 0
            commit('cart/updateItemQuantity', { id: product.id, quantity: product.stock }, { root: true })
          }
        }

        return product
      })
      commit('setProducts', data)
    })
  },
  setRate: ({ commit }) => {
    const rate = Math.floor(Math.random() * 61) + 20
    commit('setRate', rate)
  }
}

const mutations = {
  setProducts (state, data) {
    state.all = data
  },
  setRate (state, data) {
    state.rate = data
  },
  pushProductInventory (state, { id, quantity }) {
    const product = state.all.find(product => product.id === id)
    product.inventory = product.inventory + quantity
  },
  incrementProductInventory (state, { id }) {
    const product = state.all.find(product => product.id === id)
    product.inventory++
  },
  decrementProductInventory (state, { id }) {
    const product = state.all.find(product => product.id === id)
    product.inventory--
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
