# Shopping cart

## Основные действия

### Нормализация данных
После сопоставления данных из файла data.json и names.js, товар имеет следующую структуру:
- id
- title - заголовок
- category - название категории
- price - цена
- stock - остаток на складе
- inventory - доступный остаток

### Алгоритм работы остатков товара
- stock - фактическое количество товара на складе
- inventory - доступный остаток товара для добавления в корзину
- quantity - количество товара добавленного в корзину

При добавлении товара в корзину уменьшается inventory, увеличивается quantity, stock не изменяется.

После получения новых данных (каждые 15 секунд) происходит проверка товаров в корзине.

Если есть товар в корзине и stock больше чем quantity, то inventory становится равным разнице stock и quantity. Если stock меньше или равно quantity, то inventory присваивается 0, а quantity присваивается stock.

Рядом с названием товара выводится stock.

'Количество ограничено' показывается когда inventory меньше 10.

### Обновление цен
Для анимации обновления цен используется библиотека GSAP

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
